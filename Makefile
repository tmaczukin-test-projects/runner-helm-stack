NAMESPACE ?= runner-hpa-test
NAME ?= runner-helm-stack
CHART = .

REGISTRATION_TOKEN ?= ""

.PHONY: namespace
namespace:
	@kubectl get namespace $(NAMESPACE) || kubectl create namespace $(NAMESPACE)
	@which kubens >/dev/null && kubens $(NAMESPACE) || echo "No kubens"

.PHONY: install
install: namespace
	@helm install \
		--namespace $(NAMESPACE) \
		--name $(NAME) \
		--set gitlab-runner.runnerRegistrationToken=$(REGISTRATION_TOKEN) \
		$(CHART)

.PHONY: upgrade
upgrade:
	@helm upgrade \
		--namespace $(NAMESPACE) \
		--force \
		--recreate-pods \
		--set gitlab-runner.runnerRegistrationToken=$(REGISTRATION_TOKEN) \
		$(NAME) \
		$(CHART)

.PHONY: delete
delete:
	@helm delete --purge $(NAME) || echo "No release $(NAME)?"
	@kubectl delete namespace $(NAMESPACE) || echo "No namespace $(NAMESPACE)?"
	@which kubens >/dev/null && kubens default

.PHONY: status
status:
	@helm status $(NAME)

.PHONY: proxy-prometheus
proxy-prometheus:
	@kubectl port-forward \
		     --namespace $(NAMESPACE) \
			 $(shell kubectl get pods --namespace $(NAMESPACE) -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}") \
			 9090

.PHONY: get-raw-metrics
get-raw-metrics:
	@kubectl get --raw \
		     /apis/custom.metrics.k8s.io/v1beta1/namespaces/runner-hpa-test/pods/*/gitlab_runner_concurrent_saturation | jq .

.PHONY: hpa-status
hpa-status:
	@kubectl get hpa/$(NAME)-gitlab-runner


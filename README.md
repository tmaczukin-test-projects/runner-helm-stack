# GitLab Runner deployment with HPA test

This project contains an example configuration of GitLab Runner K8S deployment,
using official [GitLab Runner Helm Chart](https://gitlab.com/charts/gitlab-runner)
with support for [Horizontal Pod Autoscaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/).

## Overwiev

GitLab CI architecture is designed in a highly distributed and parallel way. The job execution part - GitLab Runner
- is prepared to be registered multiple times, to support many type of environments but also to support higher
CI load values.

While GitLab Runner supports executors that by themselves have autoscaling mechanisms ([Kubernetes
executor](https://docs.gitlab.com/runner/executors/kubernetes.html), [Docker
Machine executor](https://docs.gitlab.com/runner/executors/docker_machine.html)), it may happen that the Runner process
itself will reach it limits. It may be a limit specified by the user in the configuration - like the `concurrent`
setting. But it may be also as simple as a CPU utilization hitting the roof on the machine, where GitLab Runner process
is running.

For Runners installed on-premisses _"manually"_ (and by _"manually"_ I understand here also the installation by GitLab
Runner official DEB/RPM packages or manual installation of official `gitlab/gitlab-runner` Docker image) the user must
resolve this problem by himself.

But we can also use Kubernetes to deploy the GitLab Runner processes themselves (please, don't confuse this with using
Kubernetes executor of GitLab Runner which is responsible for **executing the jobs** on a K8S cluster). And with
Kubernetes as the deployment environment, we can take all of the advantages of _Horizontal Pod Autoscaling_ feature,
that is core feature of K8S.

The easiest way to install GitLab Runner on a K8S cluster is to use the official Helm Chart. And thanks to a community
contributor, it will soon get a support for HPA configuration - please follow https://gitlab.com/charts/gitlab-runner/merge_requests/127
for the details.

Using this, one can define Runner configuration, define minimal and maximal number of replicas (read: how many Runner
containers with GitLab Runner processes will be working there) and define what metrics to track to decide when the
deployment should be scalled up or down.

Thanks to this, we can have a deployment that will automatically align to the current load, up to the limits defined by
the user. Let's look on real-life example:

Using configuration hosted in this project, I've prepared an environment with GitLab Runner configured to handle up to
10 concurrent jobs. Next, in a project in which this Runner was registered, I've started ~60 jobs. First 10 jobs were
quickly scheduled which fully saturated available concurrency.

![Jobs number graph](screens/runner-jobs-number.png)

This is where HPA starts it work - it's constantly checking the metrics and it discovered, that the configured metric
exceeded the threshold. Seeing what is the current value of the metric and what is the autoscaling configuration, HPA
autoscaling algorithm decided to scale the deployment and increase the replicas number. The situation was repeated
multiple times, until the maximum limit of 10 replicas was reached.

![Concurrency saturation](screens/runner-concurrency-saturation.png)

When the last jobs started finishing, the saturation metrics started dropping down. After a while (which is longer for
the scale-down) HPA again recognized that the number of replicas is not fitting best the current load and it started
scalling the deployment down. And this was being repeated until the deployment reached the minimum limit of 1 replicas.

![Number of replicas in the deployment](screens/runner-deployment-replicas-number.png)

## Preparation and setup

> **NOTICE:**
>
> The configuration here relays on currently [unmerged MR](https://gitlab.com/charts/gitlab-runner/merge_requests/127)
> adding support for HPA to the GitLab Runner Helm Chart. The MR is in the process of being reviewed, so some details
> may need to be updated after it will finally be merged.
>
> The configuration here relays also on one fix in GitLab Runner - https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1506.
> Unfortunately, HPA doesn't work well with scaling down when the source metric may disappear (which is not uncommon for
> gauges like `gitlab_runner_jobs`), and for now this seems to be the easiest way to fix the problem.

To test this configuration we need to prepare few things:

1. Install and configure access to the K8S cluster.
1. Install and initialize Helm.
1. Prepare the GitLab Runner Helm Chart used by this configuration (since we're relaying on currently unmerged change).
1. (optional) Adjust the configuration of GitLab Runner deployment.

But before - make sure that `make` is installed ;)

### K8S cluster setup

To test this configuration we obviously need a Kubernetes cluster. And there are plenty of options to acquire one.

The simplest one will be to use [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/). It will
install K8S locally on our machine, using one of the supported Hypervisors (with VirtualBox being the default one).
It will also prepare configuration for `kubectl` to allow you easily operate the cluster from your host machine.

You can also choose one of the hosted clusters, like [Google's GKE](https://cloud.google.com/kubernetes-engine/),
[Amazon's EKS](https://aws.amazon.com/eks/), [DigitalOcean
Kubernetes](https://www.digitalocean.com/products/kubernetes/) and many, many more. You may choose whatever provider you
want, but remember that hosted K8S usually means a cost. However, most of the cloud providers should support a trial
plan with some credit for evaluation. Decision is up to you! :)

When the K8S cluster will be ready, we also need to [install and configure `kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl/),
so we will be able to interact with the cluster.

For Bash users it may be also interesting to install [`kubectx`+`kubens`](https://kubectx.dev/) which simplifies
switching between K8S contextes and namespaces.

Having all of the required tools installed, we can go to the next step...

### Helm installation and initialization

...which is to install and initialize [Helm](https://helm.sh/).

What is Helm? It's a packaging mechanism for Kubernetes. Helm allows one to prepare a set of parametrized templates for
Kubernetes manifests, which can be nest easily installed, updated and uninstalled from the K8S cluster.

But to use helm, we first need to install it on our machine and initialize it's interaction with our K8S cluster.

[There are several ways](https://github.com/helm/helm#install) to install Helm. And when Helm itself will be installed
the next step that we need to do, is to install Tiller - Helm's server that manages deployments on K8S cluster - on the
cluster. Details on how to do this [can be also found](https://helm.sh/docs/using_helm/#installing-tiller) in Helm's
documentation.

Having `helm` tool installed and working on our machines and Tiller deployed to the K8S cluster, we can go forward.

### GitLab Runner Helm Chart preparation

As mentioned above, this configuration uses a feature that wasn't yet merged into GitLab Runner Helm Chart `master`
branch. Because of that we need to manually prepare the Chart, even if the `requirements.yaml` already specifies
`gitlab/gitlab-runner` as a dependency.

But first, let's install all the dependencies, so our custom build of GitLab Runner Helm Chart will not be overwriten.

To install the dependencies we need to first clone the project, and next enter the local working copy request
dependncies download:

```bash
$ cd /tmp

$ git clone https://gitlab.com/tmaczukin-test-projects/runner-helm-stack
Cloning into 'runner-helm-stack'...
(...)

$ cd runner-helm-stack

$ helm dep build
Hang tight while we grab the latest from your chart repositories...
...Unable to get an update from the "local" chart repository (http://127.0.0.1:8879/charts):
        Get http://127.0.0.1:8879/charts/index.yaml: dial tcp 127.0.0.1:8879: connect: connection refused
...Successfully got an update from the "gitlab" chart repository
...Successfully got an update from the "k8s-gcp" chart repository
...Successfully got an update from the "stable" chart repository
Update Complete. ⎈Happy Helming!⎈
Saving 3 charts
Downloading gitlab-runner from repo https://charts.gitlab.io/
Downloading prometheus from repo https://kubernetes-charts.storage.googleapis.com
Downloading prometheus-adapter from repo https://kubernetes-charts.storage.googleapis.com
Deleting outdated charts
```

This will get the dependencies specified in `requirements.lock` and install them in the `charts/` directory. After
executing this, `charts/` directory should look more or less like:

```bash
$ ls -l charts/
total 44
-rw-r--r-- 1 tmaczukin tmaczukin 10197 lip 30 13:13 gitlab-runner-0.7.0.tgz
-rw-r--r-- 1 tmaczukin tmaczukin 22603 lip 30 13:13 prometheus-8.14.3.tgz
-rw-r--r-- 1 tmaczukin tmaczukin  7040 lip 30 13:13 prometheus-adapter-1.2.0.tgz
```

We see `prometheus` and `prometheus-adapter` installed in required versions. We can see also `gitlab-runner` chart, but
this one we will need to replace with manually created one.

To build the chart manually, we need to clone the proper repository, switch to the branch adding the feature and use
helm:

```bash
$ cd /tmp

$ git clone https://gitlab.com/dinidus/gitlab-runner
Cloning into 'gitlab-runner'...
warning: redirecting to https://gitlab.com/dinidus/gitlab-runner.git/
remote: Enumerating objects: 957, done.
remote: Counting objects: 100% (957/957), done.
remote: Compressing objects: 100% (339/339), done.
remote: Total 957 (delta 623), reused 940 (delta 614)
Receiving objects: 100% (957/957), 153.10 KiB | 320.00 KiB/s, done.
Resolving deltas: 100% (623/623), done.

$ cd gitlab-runner/

$ git checkout auto-scale-runers
Branch 'auto-scale-runers' set up to track remote branch 'auto-scale-runers' from 'origin'.
Switched to a new branch 'auto-scale-runers'

$ helm package .
Successfully packaged chart and saved it to: /tmp/gitlab-runner/gitlab-runner-0.6.0-beta.tgz

$ ls -l gitlab-runner-0.6.0-beta.tgz
-rw-r--r-- 1 tmaczukin tmaczukin 10141 lip 30 13:22 gitlab-runner-0.6.0-beta.tgz
```

We've built the package. Now we need to copy it to our local `charts/` directory:

```bash
$ cd /tmp/runner-helm-stack

$ rm charts/gitlab-runner-0.7.0.tgz

$ mv /tmp/gitlab-runner/gitlab-runner-0.6.0-beta.tgz charts/

$ ls -l charts/
total 44
-rw-r--r-- 1 tmaczukin tmaczukin 10141 lip 30 13:22 gitlab-runner-0.6.0-beta.tgz
-rw-r--r-- 1 tmaczukin tmaczukin 22603 lip 30 13:25 prometheus-8.14.3.tgz
-rw-r--r-- 1 tmaczukin tmaczukin  7040 lip 30 13:25 prometheus-adapter-1.2.0.tgz
```

We can see now, that the `gitlab-runner` chart is replaced with our custom build. We can move forward.

### Adjusting the configuration of GitLab Runner deployment

This is the moment to adjust the configuration of GitLab Runner, if it's needed for custom case. Runner is configured
in `values.yaml` under the `gitlab-runner:` scope:

```yaml
# ./values.yaml

gitlab-runner:
  image: gitlab/gitlab-runner:alpine-make-gitlab-runner-jobs-metric-to-be-always-available
  rbac:
    create: true
  gitlabUrl: https://gitlab.com/
  concurrent: 10
  checkInterval: 3
  (...)
```

To find out what and how can be configured, please check [the `values.yaml`
file](https://gitlab.com/charts/gitlab-runner/blob/v0.7.0/values.yaml) in GitLab Runner Helm Chart project.

Having this done, we can finally deploy the Runner!

## Installing

Or maybe not! :)

We still need to configure one more important value - registration token. GitLab Runner Helm Chart, when starting the
Runner pod, by default registers the Runner using GitLab URL provided in the `values.yaml` file. For obvious reasons
(security!) I didn't commit token for my personal test project into this repository. Instead the `Makefile` is prepared
to handle a specific environment variable: `REGISTRATION_TOKEN`. To learn how to obtain the token, please follow
https://docs.gitlab.com/ee/ci/runners/.

Assuming that the registration token for our test project is `MyToKeN1234`, we need to export the variable:

```bash
$ set +o history && \
  export REGISTRATION_TOKEN="MyToKeN1234" && \
  set -o history
```

The additional `set +o history` and `set -o history` around the `export` command will ensure, that the token value will
be not remembered in the shell history (be aware it's a feature tested on BASH - it may not work on your shell if you're
using something different).

You may also look into `Makefile` to find other parameters that can be configured with variables (`NAMESPACE` - defined
the namespace that will be created in K8S and used for the deployment; `NAME` - the name of the Helm _release_ which is
how Helm names the unique installation of given Chart).

If we're satisfied with the setting, then the only thing left is to install the Chart:

```bash
$ make install
namespace/runner-hpa-test created
Context "minikube" modified.
Active namespace is "runner-hpa-test".
NAME:   runner-helm-stack
LAST DEPLOYED: Tue Jul 30 13:44:46 2019
NAMESPACE: runner-hpa-test
STATUS: DEPLOYED

RESOURCES:
==> v1/ClusterRoleBinding
NAME                                      AGE
prometheus-adapter:system:auth-delegator  1s
prometheus-adapter-resource-reader        1s
prometheus-adapter-hpa-controller         1s

==> v1/RoleBinding
NAME                            AGE
prometheus-adapter-auth-reader  1s

==> v1/Deployment
NAME                                  DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
runner-helm-stack-prometheus-adapter  1        1        1           0          1s

==> v2beta1/HorizontalPodAutoscaler
NAME                             REFERENCE                                   TARGETS         MINPODS  MAXPODS  REPLICAS  AGE
runner-helm-stack-gitlab-runner  Deployment/runner-helm-stack-gitlab-runner  <unknown>/750m  1        10       0         1s

==> v1/Pod(related)
NAME                                                             READY  STATUS             RESTARTS  AGE
runner-helm-stack-prometheus-node-exporter-l92c8                 0/1    ContainerCreating  0         1s
runner-helm-stack-gitlab-runner-dbc5d55d-74kcv                   0/1    Init:0/1           0         1s
runner-helm-stack-prometheus-adapter-849974bd4d-vgctn            0/1    Pending            0         1s
runner-helm-stack-prometheus-alertmanager-99ff6c5b7-4sd7k        0/2    Pending            0         1s
runner-helm-stack-prometheus-kube-state-metrics-78fbc45968kb6jr  0/1    Pending            0         1s
runner-helm-stack-prometheus-pushgateway-7f4b8c9bb8-2rcrj        0/1    Pending            0         1s
runner-helm-stack-prometheus-server-6849f98f69-z746h             0/2    Pending            0         1s

==> v1/PersistentVolumeClaim
NAME                                       STATUS   VOLUME                                    CAPACITY  ACCESS MODES  STORAGECLASS  AGE
runner-helm-stack-prometheus-alertmanager  Pending  standard                                  1s
runner-helm-stack-prometheus-server        Bound    pvc-8bbdad3c-b37c-11e9-9aa4-e44734a3f6de  8Gi  RWO  standard  1s

==> v1beta1/ClusterRole
NAME                                             AGE
runner-helm-stack-prometheus-kube-state-metrics  1s
runner-helm-stack-prometheus-server              1s

==> v1beta1/Role
NAME                             AGE
runner-helm-stack-gitlab-runner  1s

==> v1beta1/Deployment
NAME                                             DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
runner-helm-stack-gitlab-runner                  1        1        1           0          1s
runner-helm-stack-prometheus-alertmanager        1        1        1           0          1s
runner-helm-stack-prometheus-kube-state-metrics  1        1        1           0          1s
runner-helm-stack-prometheus-pushgateway         1        1        1           0          1s
runner-helm-stack-prometheus-server              1        1        1           0          1s

==> v1/ConfigMap
NAME                                       DATA  AGE
runner-helm-stack-gitlab-runner            5     1s
runner-helm-stack-prometheus-adapter       1     1s
runner-helm-stack-prometheus-alertmanager  1     1s
runner-helm-stack-prometheus-server        3     1s

==> v1/ClusterRole
NAME                                 AGE
prometheus-adapter-server-resources  1s
prometheus-adapter-resource-reader   1s

==> v1beta1/RoleBinding
NAME                             AGE
runner-helm-stack-gitlab-runner  1s

==> v1beta1/DaemonSet
NAME                                        DESIRED  CURRENT  READY  UP-TO-DATE  AVAILABLE  NODE SELECTOR  AGE
runner-helm-stack-prometheus-node-exporter  1        1        0      1           0          <none>         1s

==> v1beta1/APIService
NAME                           AGE
v1beta1.custom.metrics.k8s.io  1s

==> v1/ServiceAccount
NAME                                             SECRETS  AGE
runner-helm-stack-gitlab-runner                  1        1s
runner-helm-stack-prometheus-adapter             1        1s
runner-helm-stack-prometheus-alertmanager        1        1s
runner-helm-stack-prometheus-kube-state-metrics  1        1s
runner-helm-stack-prometheus-node-exporter       1        1s
runner-helm-stack-prometheus-pushgateway         1        1s
runner-helm-stack-prometheus-server              1        1s

==> v1beta1/ClusterRoleBinding
NAME                                             AGE
runner-helm-stack-prometheus-kube-state-metrics  1s
runner-helm-stack-prometheus-server              1s

==> v1/Service
NAME                                             TYPE       CLUSTER-IP     EXTERNAL-IP  PORT(S)   AGE
runner-helm-stack-prometheus-adapter             ClusterIP  10.98.19.197   <none>       443/TCP   1s
runner-helm-stack-prometheus-alertmanager        ClusterIP  10.107.46.68   <none>       80/TCP    1s
runner-helm-stack-prometheus-kube-state-metrics  ClusterIP  None           <none>       80/TCP    1s
runner-helm-stack-prometheus-node-exporter       ClusterIP  None           <none>       9100/TCP  1s
runner-helm-stack-prometheus-pushgateway         ClusterIP  10.96.15.198   <none>       9091/TCP  1s
runner-helm-stack-prometheus-server              ClusterIP  10.107.12.210  <none>       80/TCP    1s
```

Notice that Helm provides you a status output for the installed _release_. You may check it at any moment by running
`make status`. To apply any configuration changes please use `make upgrade`. And to remove the release, simply call
`make delete`.

## Testing

To test if the autoscaling is working we need two things: a way to schedule tens of jobs and a way to observer the
behavior of the system.

For observation we could constantly call `make status` or even `kubectl` directly to check if additional replicas of
Runner pods are scheduled. But with our configuration we're installing the Prometheus server that provides data for
autoscaling. So why not to use it, just like HPA does! Additionally, we will be able to generate graphs similar to the
ones printed above.

To access Prometheus installed within our release, we need just to call:

```bash
$ make proxy-prometheus
Forwarding from 127.0.0.1:9090 -> 9090
Forwarding from [::1]:9090 -> 909
```

This will create a proxy, that will forward the traffic for `http://127.0.0.1:9090/` to the Prometheus service. Just be
aware, that K8S may need a moment to start the Prometheus pod and make the service available. In case if you will see an
error similar to:

```bash
$ make proxy-prometheus
error: unable to forward port because pod is not running. Current status=Pending
make: *** [Makefile:42: proxy-prometheus] Error 1
```

you should wait couple of seconds and try again.

To be sure that Prometheus is up and it's tracking GitLab Runner's exporter, open http://127.0.0.1:9090/targets#job-kubernetes-pods
and confirm that we have an endpoint listening on port `9252` (in my case it's `http://172.17.0.15:9252/metrics`) that
is labeled with `app="runner-helm-stack-gitlab-runner"`, where `runner-helm-stack` references the release name, and `gitlab-runner`
the Runner chart.

If it works, then we gan switch to [Graph](http://127.0.0.1:9090/graph) tab, add two more graphs and execute three
queries:

1. `sum(gitlab_runner_jobs)` - this one will show how many jobs Runner processes are currently processing. This is what
   generates load and creates the need to autoscale.

1. `sum(gitlab_runner:concurrent_saturation) / sum(gitlab_runner_version_info)` - this will show how much Runner's
   concurrency (defined by `concurrent` setting) is saturated. This is what controls the autoscaler.

1. `sum(gitlab_runner_version_info) by (version)` - this will show how many Runner processes (pod replicas) are
   existing. This will present the status of autoscaler.

For all three queries we should choose the `Graph` tab, so we will look how this was changing in time, instead of seeing
only the latest value for the measurement. If the proxy was set up as described above, all three graphs should be
visible under [this URL](http://127.0.0.1:9090/graph?g0.range_input=1h&g0.expr=sum(gitlab_runner_jobs)&g0.tab=0&g1.range_input=1h&g1.expr=sum(gitlab_runner%3Aconcurrent_saturation)%20%2F%20sum(gitlab_runner_version_info)&g1.tab=0&g2.range_input=1h&g2.expr=sum(gitlab_runner_version_info)%20by%20(version)&g2.tab=0).

Having the way to preview what is the state of the environment, we can finally try to test it. For this simply create a
lot of jobs in your test project (the one you've used for Runner registration). The jobs should be configured to use
the Runner that we've deployed with this configuration.

While testing this configuration I was using this project: https://gitlab.com/tmaczukin-test-projects/test-k8s-hpa-support-in-helm-chart

It starts several jobs in one pipeline. To test autoscaler behavior we can just enter the new Pipeline form
(in my project it would be https://gitlab.com/tmaczukin-test-projects/test-k8s-hpa-support-in-helm-chart/pipelines/new)
and start the pipeline on `master` several times.

After the pipelines are started, we can start executing the queries in Prometheus UI, to see how the graph is changing
in time.

And voilà! If everything went good, we should start seeing that the deployment is scaled up when there is a lot of jobs
processed by Runner, and when the numner of jobs starts to decrease, we should also see a decreasing level of
_concurrency saturation_ and in the end - decreasing number of Runner pods.

## Author

Tomasz Maczukin, 2019

## License

MIT - for the code

CC BY-SA 4.0 - for the documentation part

